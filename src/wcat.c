#include <stdio.h>
#include <stdlib.h>
void catfile (char filetoopen[]) {

    FILE *file;
        file = fopen(filetoopen, "r");
    
        if (file != NULL) {
    
            fseek(file, 0, SEEK_END);
            long filelength;
            filelength = ftell(file);
            if (filelength == -1L) {
            
                perror("Error in file length code");
            
            }
    
            char *filecontents = (char *)malloc(filelength + 1);
            filecontents[filelength] = '\0';
            rewind(file);
    
            while ( fgets(filecontents, filelength + 1, file) ) {
    			
    			printf("%s", filecontents);
                        
    	    }
    
            fclose(file);
            free(filecontents);
    
        } else {
    
            fprintf(stderr, "Something went wrong. Try checking file permissions.");
    
        }
        
}

int main (int argc, char *argv[]) {

    switch (argc) {
        case 1: {

            while (1) {
            
                char a = getchar();
                if ( a != EOF ) { putchar(a); } else { return 0; }

            }
        } case 2: {
        
            catfile(argv[1]);
            return 0;
        
        } default: {

            fprintf(stderr, "Too many arguments");
            return 1;
        
        }
        
    }
    
}

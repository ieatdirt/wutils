#include <stdio.h>
#include <stdlib.h>
int copyfile (char fileinput[], char fileoutput[]) {

    FILE *filein;
    FILE *fileout;
        filein = fopen(fileinput, "r");
        fileout = fopen(fileoutput, "w");
    
        if (filein != NULL && fileout != NULL) {
    
            fseek(filein, 0, SEEK_END);
            long filelength;
            filelength = ftell(filein);
            if (filelength == -1L) {
            
                perror("Error at line 10-19");
            
            }
    
            char *filecontents = (char *)malloc(filelength + 1);
            filecontents[filelength] = '\0';
            rewind(filein);
    
            while ( fgets(filecontents, filelength+1, filein) ) {
    			
    			fprintf(fileout, "%s", filecontents);
                        
    	    }
    
            fclose(filein);
            fclose(fileout);
            free(filecontents);
            return 0;
    
        } else {
    
            perror("Permission Denied\n");
            return 1;
    
        }
        
}

int main (int argc, char *argv[]) {

    switch (argc) {
        case 1: {
    
            puts("No arguments");
            return 1;
    
        } case 2: {
        
            puts("Too few outputs, what file are you trying to output to?, be warned, wcp WILL destroy ANY data in the out location");
            return 1;
        
        } case 3: {
        
            if ( copyfile(argv[1], argv[2]) == 0) { return 0;} else {return 1;}
        
        } default: {

            puts("Too many arguments, Proper usage is `wcp [file in] [file out]`, be warned, wcp WILL destroy ANY data in the out location");
            return 1;
        
        }
    }
}

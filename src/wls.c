#include <errno.h>
#include <stdio.h>
#include <dirent.h>
int dirlist(char inp[]) {

	DIR *directory;
	directory = opendir(inp);
	struct dirent *conts;
			
	if(directory != NULL) {

		while( ( conts = readdir(directory) ) ) {
					printf("%s ", conts->d_name);
				}
				
	} else if (errno == ENOTDIR) {
		fprintf(stderr, "%s is not a directory.\n", inp);
		return 1;
	} else if (errno == EACCES) {
		fprintf(stderr, "%s is not readable due to invalid permissions.\n", inp);
		return 1;
	} else {
		fprintf(stderr, "Cannot open %s for unknown reason.\n", inp);
		return 1;
	}

	putc('\n', stdout);
	closedir(directory);
	return 0;
}



int main(int argc, char *argv[]) {
	
	switch (argc) {
	
		case 1: 
		{
			//0 args
			dirlist("./");
			return 0;
		}
		
		case 2:
		{
			//one argument 
			if ( dirlist(argv[1]) == 1 ) {
					return 1;
			}
			return 0;
		}
		 
		default:
		{
			//more than 1 argument
			for (int i = 1; i < argc; i++) {
				
				if ( dirlist(argv[i]) == 1 ) {
					return 1;
				}

			}
			return 0;
		}
		
	}
	
}
